/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  TouchableOpacity,
  View,
  ViewPagerAndroid,
} from 'react-native';

let arrPage = [
  {title: "F Page",bgColor:'yellow'},
  {title: "S Page",bgColor:'green'},
  {title: "T Page",bgColor:'red'},
]

class IconViewPager extends Component{
render(){
  return(
     <TouchableOpacity style={[{backgroundColor: this.props.index === this.props.i ?'white' :`${this.props.bgColor}`},styles.btnOpacity]} onPress={this.props.onPress}>
       <Text>{this.props.title}</Text>
      </TouchableOpacity>
  );
}
}
class Page extends Component{
  render(){
    return(
      <View style={[{backgroundColor:`${this.props.bgColor}`}, styles.pageStyle]}>
        <Text>{this.props.title}</Text>
      </View>
    );
  }
}
export default class demo_redux_natvie extends Component {
  constructor(props){
    super(props);
    this.state = {
      index: 0
    }
  }
  
  render() {
    var pages = [];
    var bottoms = [];
    for (var i = 0; i < arrPage.length; i++) {
      pages.push(
        <Page key={i} title={arrPage[i].title} bgColor={arrPage[i].bgColor}/>
      );
    }
    for (var i = 0; i < arrPage.length; i++) {
      let index = i;
      bottoms.push(
        <IconViewPager key={i} i={index} title={arrPage[i].title} index={this.state.index} bgColor={arrPage[i].bgColor} onPress={()=>{
          console.log(i);
          this.viewPager.setPage(index);
          this.setState({index:index});
          }}/>
      );
    }
    return (
      <View style={styles.conntaner}>
            <ViewPagerAndroid
              style={styles.viewPager}
              initialPage={0}
              onPageSelected={(val)=>{
                console.log(val.nativeEvent.position);
                this.setState({index: val.nativeEvent.position});
                }}
              onPageScrollStateChanged={(val)=>{console.log(val)}}
              scrollEnabled={true}
              onPageScroll={(val)=>{console.log(val.nativeEvent)}}
              ref={viewPager => { this.viewPager = viewPager; }}
              >
              {pages}
            </ViewPagerAndroid>
            <View style={styles.appBottom}>
             {bottoms}
            </View>
      </View>
     
    );
  }
}

const styles = StyleSheet.create({
  btnOpacity:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  conntaner:{
    flex: 1
  },
  viewPager:{
    flex: 1,
    backgroundColor: 'white'
  },
  appBottom:{
    height: 40,
    flexDirection: 'row'
  },
  pageStyle: {
    alignItems: 'center',
    padding: 20,
  }
});

AppRegistry.registerComponent('demo_redux_natvie', () => demo_redux_natvie);
